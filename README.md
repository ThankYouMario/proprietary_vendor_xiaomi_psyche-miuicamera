# MIUI Leica Camera for Xiaomi 12X (Psyche)

Prebuilt MIUI Leica Camera to include in custom ROM builds

### How to use?

1. Clone this repo to `vendor/xiaomi/psyche-miuicamera`

2. Inherit it from `device.mk` in device tree:
 
```
# MIUI Leica Camera
$(call inherit-product-if-exists, vendor/xiaomi/psyche-miuicamera/products/miuicamera.mk)
```
